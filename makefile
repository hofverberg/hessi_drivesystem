# g++ -std=c++0x -c thread.cpp
# g++ -std=c++0x thread.o -o thread -lpthread


CC = g++
CFLAGS = -Wall -std=c++0x 
INCLUDES = -I./include/ -I/usr/include/mysql/ 
LFLAGS = 
LIBS = -lgtest -lpthread 

ifdef HESSROOT
INCLUDES += -I$(HESSUSER)/include/ -I$(HESSROOT)/include/ 
LFLAGS += -L$(HESSUSER)/lib/ -L$(HESSROOT)/lib/ 
LIBS += -lsimpletable
endif

OUTDIR  := out
SRCDIR  := src
TESTDIR := gtest
INCDIR  := include
BINDIR  := bin

MODULES := Utils GenericConnection ConnectionGuard TelescopeSimulator stcp SocketConnection BitField IOChannel IOModule IOModuleCommand IOModuleCommands IOModuleCommandFactory IOModuleAnswer IOModuleAnswers IOModuleAnswerFactory 
TESTMODULES := TestConnectionGuard  TestBitField TestIOChannel TestIOModule TestSocketConnection TestIOModuleCommands TestIOModuleAnswer TestUtils gtest_main

ifdef HESSROOT
MODULES += DBHandler DigitalIOHandler
TESTMODULES += TestDBHandler TestDigitalIOHandler
endif

HEADERS     := $(addsuffix .hh,$(addprefix $(INCDIR)/,$(MODULES)))
SOURCES     := $(addsuffix .C,$(addprefix $(SRCDIR)/,$(MODULES)))
TESTSOURCES := $(addsuffix .C,$(addprefix $(TESTDIR)/,$(TESTMODULES)))
OBJECTS     := $(addsuffix .o,$(addprefix $(OUTDIR)/,$(MODULES))) $(addsuffix .o,$(addprefix $(OUTDIR)/,$(TESTMODULES)))
EXECUTABLES := $(BINDIR)/runUnitTests


all: checkdirs $(EXECUTABLES)

$(EXECUTABLES): $(OBJECTS) 
	$(CC) $(CFLAGS) $(INCLUDES) -o $(EXECUTABLES) $(OBJECTS) $(LFLAGS) $(LIBS)

$(OUTDIR)/%.o: $(SRCDIR)/%.C
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@

$(OUTDIR)/%.o: $(TESTDIR)/%.C
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@

checkdirs: $(OUTDIR) $(BINDIR)

$(OUTDIR):
	@mkdir -p $@

$(BINDIR):
	@mkdir -p $@

clean:
	@rm -rf $(OUTDIR)
	@rm -rf $(BINDIR)
