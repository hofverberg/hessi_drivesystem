/**
   \class tracking::BitField

   A container class for a bitfield. The bitfield can be initialized as, or
   converted to, a
   1) uint32_t
   2) std::vector<bool> (where 'true' corresponds to a bit set to 1, vector[0] =
   LSB)
   3) std::vector<char> (where char is a hex character, vector[0] = MSC)
   4) std::string (where the string contains hex characters, string[0] = MSC)

   \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_BITFIELD_H_
#define ONLINETRACKING_BITFIELD_H_

#include <stdint.h>
#include <vector>

#include "Constants.hh"
#include "Utils.hh"

namespace tracking {

class BitField {
 public:
  /// Create a bitfield from a uint32_t integer
  explicit BitField(const uint32_t &value_as_int);
  /// Create a bitfield from a vector of hex (0-9,A-F) characters.
  /// Lower and Upper case characters can be given.
  /// If a non-hex character is given the constructor will throw.
  /// If the value that the vector of hex chars represent is larger than
  /// what can be stored in the bitfield, the constructor will throw.
  explicit BitField(const std::vector<char> &value_as_vector_of_hex_chars);
  /// Create a bitfield from a vector of booleans.
  /// If the value that the vector of hex chars represent is larger than
  /// what can be stored in the bitfield, the constructor will throw.
  explicit BitField(const std::vector<bool> &value_as_vector_of_bool);
  ~BitField();
  const uint32_t GetAsInt() const;
  /// Get the bit field as a std::string of hex characters.
  /// The string is padded with '0':s up to the maximum number of bits stored.
  const std::string GetAsStringOfHex() const;
  /// Get the bit field as a std::string of hex chararacter with
  /// 'number_of_hex_chars' characters. If the value cannot be represented by
  /// this number of characters the function will throw.'
  const std::string GetAsStringOfHex(const std::size_t &number_of_hex_chars)
      const;
  /// Get the bit field as a vector of booleans. The vector is padded with
  /// 'false':s up to the maximum number of bits stored.
  const std::vector<bool> GetAsVectorOfBool() const;
  /// Get the bit field as a vector of booleans with 'number_of_bits' entries.
  /// If the value cannot be represented by this number of bits the function
  /// throws.
  const std::vector<bool> GetAsVectorOfBool(const std::size_t &number_of_bits)
      const;
  /// Get the bitfield as a vector of (uppercase) hex characters
  const std::vector<char> GetAsVectorOfHexChars() const;
  /// Get the bitfield as a vector of (uppercase) hex characters with
  /// 'number_of_hex_chars' entries. If the bitfield cannot be represented
  /// by this number of hex characters, the function throws.
  const std::vector<char> GetAsVectorOfHexChars(
      const std::size_t &number_of_hex_chars) const;
  /// Get the bitfield as a vector of (uppercase) hex characters with the
  /// minimum number of entries (i.e no padding '0's).
  const std::vector<char> GetAsMinimalVectorOfHexChars() const;

 private:
  /// Get the minimum number of hex chars that is required for representing
  /// the bitfield
  const std::size_t GetMinNumberOfRequiredHexChars() const;
  /// Get the minimum number of bits that is required for representing
  /// the bitfield
  const std::size_t GetMinNumberOfRequiredBits() const;
  /// Get the number of hex chars that is needed for representing
  /// the largest value the class can hold
  const std::size_t GetMaxNumberOfHexChars() const;
  /// Get the number of bits that is needed to represent the largest value
  /// the class can hold
  const std::size_t GetMaxNumberOfBits() const;
  /// Get the number of bits required to represent a hex character
  const std::size_t GetNumberOfBitsInAHexChar() const { return 4; }

  uint32_t value_;  /// The variable by which the bitfield is stored
};

}  // namespace

#endif
