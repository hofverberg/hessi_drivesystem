/**
   \class tracking::IOChannel

   Represents a channel for a digital I/O module for a CT1-4 type telescope.
   A channel is characterised by its vale, which can be 0 or 1, and its logic,
   which can be either positive (=1) or negative (=0), and its number, which is
   its physical location in the IOModule.

   Note the differentiation beteen channel values, which is simply the
   value stored in the module, and the notation "triggered" or "untriggered"
   channels. The latter is
   determined from the channel value and the channel logic. For example, a
   channel with a value
   of 1 with a positive logic (=1) is triggered. A channel with a value of 0
   with a
   negative logic (=0) is also triggered.

   An IOChannel is always initialized as "UnTriggered",i.e the channel value
   is set differently depending on the channel logic (see Constructor).

   \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_IOCHANNEL_H_
#define ONLINETRACKING_IOCHANNEL_H_

#include <string>

namespace tracking {

class IOChannel {
 public:
  /// Creates an IOChannel with a number, name and logic.
  /// Throws if any of them is unvalid.
  IOChannel(const int &number, const std::string &name, const int &logic);
  ~IOChannel();

  const int GetNumber() const;
  const std::string GetName() const;
  void SetValue();
  void UnSetValue();
  const bool IsValueSet() const;
  const bool IsValueUnSet() const;
  const bool HasPositiveLogic() const;
  const bool HasNegativeLogic() const;

  /// Prints the number, name and logic of the channel
  void Print() const;

 private:
  enum Value {
    Value_UnSet = 0,
    Value_Set = 1
  };

  enum Logic {
    Logic_Negative = 0,
    Logic_Positive = 1
  };

  int number_;
  std::string name_;
  Value value_;
  Logic logic_;
};

}  // namespace

#endif
