/**
 * \class tracking::TelescopeIO
 *
 * \brief Implements communication between controller, servos and IO modules
 *
 * \author Petter Hofvererg
*/

#ifndef TELESCOPEIO_H
#define TELESCOPEIO_H

#include "Constants.hh"
#include "SocketConnection.hh"
#include "TelescopeSimulator.hh"

namespace tracking {

class TelescopeIO {
 public:
  static TelescopeIO* GetInstance();
  ~TelescopeIO();

  void Connect();
  void Deconnect();
  bool IsConnected();

  void SendServoCommand(Axis axis);
  void GetServoResponse(Axis axis);
  void GetIOModule();
  void HelloWatchdog();

  void ActivateSimulationMode() { m_SimulationMode = true; }
  void DeactivateSimulationMode() { m_SimulationMode = false; }

 private:
  static TelescopeIO* m_Instance_p;
  TelescopeIO();

  Servo m_Servo[2];
  IOModules m_IOModules;

  bool m_SimulationMode;
};

}  // namespace

#endif
