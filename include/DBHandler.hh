/**
   \class tracking::DBHandler

   An interface class to the DAQ data base. Loops over a table
   specified by the user and gives back all columns that was found.
   Example: return all rows from the table "tablename" that has the
   "Telescope" field name = "1".
   \code{.cpp}
   DBHandler db_handler;
   db_handler.Connect();
   db_handler.SetTable("tablename","Telescope", 1);
   std::vector<std::string> table_row_content;
   while( db_handler.FetchNextRowFromTable(&table_row_content);
   // do something with table_row_content
   db_handler.Deconnect();
   \endcode
   \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_DBHANDLER_H_
#define ONLINETRACKING_DBHANDLER_H_

#include "dbtools/simpletable.hh"
#include "GenericConnection.hh"

namespace tracking {

class DBHandler : public GenericConnection {
 public:
  DBHandler();
  ~DBHandler();

  /// Connect to the data base with the configuration 'hessdaq'.
  /// Trows if a connection could not be established.
  void Connect();
  virtual void Deconnect();
  const bool IsConnected() const;
  /// Set the table that the class will loop over in subsequent calls to
  /// FetchNextRowFromTable.
  /// @table_name data base table
  /// @param id_field_name Select a field to filter the result by
  /// @param id_field_value Select only rows where the id_field_name parameter
  /// has the given id_field_value
  void SetTable(const std::string &table_name, const std::string &id_field_name,
                const int &id_field_value);
  /// Set the table that the class will loop over in subsequent calls to
  /// FetchNextRowFromTable. In addition to the above SetTable function,
  /// the returned data will be filtered using the auxiliary fields
  /// @param aux_field1_name filter the data using this parameter
  /// @param aux_field1_value .. that have this value
  /// @param aux_field2_name filter the data using this parameter
  /// @param aux_field2_value .. that have this value
  void SetTable(const std::string &table_name, const std::string &id_field_name,
                const int &id_field_value, const std::string &aux_field1_name,
                const std::string &aux_field1_value,
                const std::string &aux_field2_name,
                const std::string &aux_field2_value);
  /// Get the next row from the currently set table. Throws if the table is
  /// empty.
  /// @param row_content Vector of strings that will be filled with the table
  /// row content (one item per column).
  /// @return true if a row was found.
  const bool FetchNextRowFromTable(std::vector<std::string> *row_content);

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  DBHandler(const DBHandler &handler);
  DBHandler &operator=(const DBHandler &handler);

  simpletable::MySQLconnection mySQL_connection_;
  simpletable::SimpleTable *simpletable_;
};

}  // namespace

#endif
