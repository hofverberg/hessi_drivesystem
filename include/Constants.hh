/**
   \file Constants

   Global constants for the tracking package.
   All enumerations include a _First and _Last item
   to make it possible to iterate over the enumeration.

   \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_CONSTANTS_H_
#define ONLINETRACKING_CONSTANTS_H_

#include <stdlib.h>
#include <cmath>
#include <string>
#include <string>

namespace tracking {

enum TelescopeId {
  TelescopeId_Invalid = 0,
  TelescopeId_First = 1,
  TelescopeId_CT1 = 1,
  TelescopeId_CT2 = 2,
  TelescopeId_CT3 = 3,
  TelescopeId_CT4 = 4,
  TelescopeId_Last = 4
};

enum Axis {
  Axis_Invald = 0,
  Axis_First = 1,
  Axis_Az = 1,
  Axis_El = 2,
  Axis_Both = 3,
  Axis_Last = 3
};

enum ModuleType {
  ModuleType_Invalid = 0,
  ModuleType_First = 1,
  ModuleType_7053D = 1,
  ModuleType_7067 = 2,
  ModuleType_7067D = 3,
  ModuleType_Last = 3
};

enum CommandType {
  CommandType_Invalid = 0,
  CommandType_First = 1,
  CommandType_SearchModule = 1,
  CommandType_ReadModuleHardwareConfig = 2,
  CommandType_GetChannelValues = 3,
  CommandType_SetChannelValues = 4,
  CommandType_PingWatchdog = 5,
  CommandType_CheckWatchdog = 6,
  CommandType_ResetWatchdog = 7,
  CommandType_EnableWatchdog = 8,
  CommandType_DisableWatchdog = 9,
  CommandType_Last = 9
};

const int kASCII_CR = 13;

}  // namespace

#endif
