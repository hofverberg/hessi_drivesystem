/**
 * \class tracking::template
 *
 * \brief
 *
 * \author Petter Hofvererg
*/

#ifndef TEMPLATE_H
#define TEMPLATE_H

#include "Constants.hh"

namespace tracking {

class Template {
 public:
  Template();
  ~Template();

 private:
};

}  // namespace

#endif
