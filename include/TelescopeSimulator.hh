/**
  \class tracking::TelescopeSimulator

  Simulates the I/O and movement (not yet) of a real CT1-4 telescope.

  \author Petter Hofvererg
*/

#ifndef ONLINETRACKING_TELESCOPESIMULATOR_H_
#define ONLINETRACKING_TELESCOPESIMULATOR_H_

#include "SocketConnection.hh"

namespace tracking {

class TelescopeSimulator {
 public:
  TelescopeSimulator();
  ~TelescopeSimulator();

  void Connect();
  void Deconnect();
  void ReadData(std::size_t n_chars);
  void WriteData();
  void WriteNonsense(std::size_t n_chars);
  void ReadAndReply(std::size_t n_read_chars, std::vector<char> reply);
  const std::vector<char>& GetReadBuffer();

 private:
  SocketConnection socket_connection_;
  std::vector<char> data_buffer_;
};

}  // namespace

#endif
