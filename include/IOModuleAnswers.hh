/**
   \file IOModuleAnswers

   Contains all classes deriving from IOModuleAnswer.
   The common structure of all answers are inhereted from IOModuleAnswer.
   Each derived answer specifies, in greater detail, the syntax of the answer.
  This could
   be the expected start character, the length of the body, the position of the
  data inside the body.

   These classes should only be instantitated via the IOModuleAnswerFactory.

   For a description of what the commands do, see the file IOModuleCommands.C.

  \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_IOMODULEANSWERS_H_
#define ONLINETRACKING_IOMODULEANSWERS_H_

#include <vector>

#include "Constants.hh"
#include "IOModuleAnswer.hh"

namespace tracking {

/**
   \class tracking::SearchModuleAnswer

   Expected answer =  '!' + [module_address] + [module_type] + 2*checksum +
   endchar = 10 or 11 bytes depending on model type.

   [module_type] is the type of the module that was found at [module_address]
*/
class SearchModuleAnswer : public IOModuleAnswer {

 public:
  explicit SearchModuleAnswer(const std::vector<char> &answer);
  ~SearchModuleAnswer() { ; }
  virtual const std::vector<char> GetData() const;

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  SearchModuleAnswer(const SearchModuleAnswer &handler);
  SearchModuleAnswer &operator=(const SearchModuleAnswer &handler);
  const std::size_t GetExpectedReplyLength(const ModuleType &module_type) const;
  virtual const bool IsValid() const;
};

/**
   \class tracking::ReadModuleHardwareConfigAnswer

   todo
*/
class ReadModuleHardwareConfigAnswer : public IOModuleAnswer {
 public:
  explicit ReadModuleHardwareConfigAnswer(const std::vector<char> &answer);
  ~ReadModuleHardwareConfigAnswer() { ; }
  virtual const std::vector<char> GetData() const;

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  ReadModuleHardwareConfigAnswer(const ReadModuleHardwareConfigAnswer &handler);
  ReadModuleHardwareConfigAnswer &operator=(
      const ReadModuleHardwareConfigAnswer &handler);
  const std::size_t GetExpectedReplyLength() const { return 0; }
  virtual const bool IsValid() const;
};

/**
   \class tracking::GetChannelValuesAnswer

   Expected answer  = '>' + (4char HEXBITFIELD) + 2*checksum + endchar = 8 bytes

   The hex bitfield gives the state of each channel in the module (1 or 0). The
   value
   of maximum 16 channels can be given since the answer has 4 hex chars. If the
   module contains less than 16 channels,
   the remaining bits are set to 0.
*/
class GetChannelValuesAnswer : public IOModuleAnswer {
 public:
  explicit GetChannelValuesAnswer(const std::vector<char> &answer);
  ~GetChannelValuesAnswer() { ; }
  virtual const std::vector<char> GetData() const;

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  GetChannelValuesAnswer(const GetChannelValuesAnswer &handler);
  GetChannelValuesAnswer &operator=(const GetChannelValuesAnswer &handler);
  const std::size_t GetExpectedReplyLength() const { return 8; }
  virtual const bool IsValid() const;
};

/**
   \class tracking::SetChannelValuesAnswer

   Expected answer = '>' + 2*checksum + endchar = 4 bytes
   This answer has no body.
*/
class SetChannelValuesAnswer : public IOModuleAnswer {
 public:
  explicit SetChannelValuesAnswer(const std::vector<char> &answer);
  ~SetChannelValuesAnswer() { ; }
  virtual const std::vector<char> GetData() const;

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  SetChannelValuesAnswer(const SetChannelValuesAnswer &handler);
  SetChannelValuesAnswer &operator=(const SetChannelValuesAnswer &handler);
  const std::size_t GetExpectedReplyLength() const { return 4; }
  virtual const bool IsValid() const;
};

/**
   \class tracking::CheckWatchdogAnswer

   Expected answer = '!' + [module_address] + 2 hexchar + 2*checksum + endchar =
   8 bytes

   hexchar = "00" if watchdog is disabled
   hexchar = "80" if watchdog is enabled
*/
class CheckWatchdogAnswer : public IOModuleAnswer {
 public:
  explicit CheckWatchdogAnswer(const std::vector<char> &answer);
  ~CheckWatchdogAnswer() { ; }
  virtual const std::vector<char> GetData() const;

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  CheckWatchdogAnswer(const CheckWatchdogAnswer &handler);
  CheckWatchdogAnswer &operator=(const CheckWatchdogAnswer &handler);
  const std::size_t GetExpectedReplyLength() const { return 8; }
  virtual const bool IsValid() const;
};

/**
   \class tracking::ResetWatchdogAnswer

   Expected answer = '!' + [module_address] + 2*checksum + endchar = 6 bytes
*/
class ResetWatchdogAnswer : public IOModuleAnswer {
 public:
  explicit ResetWatchdogAnswer(const std::vector<char> &answer);
  ~ResetWatchdogAnswer() { ; }
  virtual const std::vector<char> GetData() const;

 private:
  //< the copy constructor and assignment operator are declared
  //< but have no definition to avoid them from being (mis)used.
  ResetWatchdogAnswer(const ResetWatchdogAnswer &handler);
  ResetWatchdogAnswer &operator=(const ResetWatchdogAnswer &handler);
  const std::size_t GetExpectedReplyLength() const { return 6; }
  virtual const bool IsValid() const;
};

/**
   \class tracking::EnableWatchdogAnswer

   Expected answer = '!' + [module_address] + 2*checksum + endchar = 6 bytes
*/
class EnableWatchdogAnswer : public IOModuleAnswer {
 public:
  explicit EnableWatchdogAnswer(const std::vector<char> &answer);
  ~EnableWatchdogAnswer() { ; }
  virtual const std::vector<char> GetData() const;

 private:
  //< the copy constructor and assignment operator are declared
  //< but have no definition to avoid them from being (mis)used.
  EnableWatchdogAnswer(const EnableWatchdogAnswer &handler);
  EnableWatchdogAnswer &operator=(const EnableWatchdogAnswer &handler);
  const std::size_t GetExpectedReplyLength() const { return 6; }
  virtual const bool IsValid() const;
};

/**
   \class tracking::DisableWatchdogAnswer

   Expected answer = '!' + [module_address] + checksum + endchar = 6 bytes
*/
class DisableWatchdogAnswer : public IOModuleAnswer {
 public:
  explicit DisableWatchdogAnswer(const std::vector<char> &answer);
  ~DisableWatchdogAnswer() { ; }
  virtual const std::vector<char> GetData() const;

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  DisableWatchdogAnswer(const DisableWatchdogAnswer &handler);
  DisableWatchdogAnswer &operator=(const DisableWatchdogAnswer &handler);
  const std::size_t GetExpectedReplyLength() const { return 6; }
  virtual const bool IsValid() const;
};

/**
   \class tracking::TestAnswer

   For unit testing of IOModuleAnswer
*/
class TestAnswer : public IOModuleAnswer {
 public:
  explicit TestAnswer(const std::vector<char> &answer);
  ~TestAnswer() { ; }
  virtual const std::vector<char> GetData() const;

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  TestAnswer(const TestAnswer &handler);
  TestAnswer &operator=(const TestAnswer &handler);
  virtual const bool IsValid() const;
};

}  // namespace

#endif
