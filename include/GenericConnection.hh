/**
   \class tracking::GenericConnection

   Base class for all types that implements a connection to external hardware.

   \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_GENERICCONNECTION_H_
#define ONLINETRACKING_GENERICCONNECTION_H_

namespace tracking {

class GenericConnection {
 public:
  GenericConnection();
  ~GenericConnection();

  virtual void Deconnect() = 0;

 private:
};

}  // namespace

#endif
