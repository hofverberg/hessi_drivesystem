/**
   \class tracking::DigitalIOHandler

   A high level interface to the 24VDC digital control signals of a
   CT1-4 type drive system and their watchdogs.

   The control signals:
   The state (triggered, untriggerd) of the individual digital control signals
   (given below in the list) can be read and manipulated.
   A triggered state means that the signal has been activated, which does NOT
   depend on the logic of the channel (which can be either positive or negative,
   but a user of this class does not need be concerned about this). The name of
   a signal reflects its triggered state. For example, if the channel "Emergency
   Stop activated" has the state triggered, it means that the emergency stop has
   really been activated. If the channel is untriggered, it has thus not been
   activated.

   The Watchdog:
   The digital io channels are equipped with a watchdog which, if it times out,
   removes the enable signal to the motors and close the brakes. This ensures
   that the telescope is always stopped if the connection to the telescope
   breaks.

   To start the watchdog with a certain timeout value (0.1s -> 25.5), use
   EnableWatchdog( timeout ). It then starts the countdown immediately and stops
   the telescope if it reaches zero.
   To reset the timer, use PingWatchdog(); the countdown then starts again from
   the value that was given when enabling it.
   The watchdog countdown can be completely stopped with DisabledWatchdog();
   To check if the watchdog has timed out (and thus stopped the telescope), use
   HasWatchdogTimedOut()

   This is the complete list of digital control signals:

   Mains Voltage Servo ELV
   Mains Voltage Servo AZM
   24 V Supply Failure
   Power Line 24 V Supply
   Emergency Stop activated
   Control Voltage 24 V OK
   Mode of Operation DC Manual active
   Mode of Operation Servo Manual active
   Mode of Operation Servo Auto active
   Endswitch Servo AZM MINUS active (ES_AZM_MINUS)
   Status PLC
   Pre-Parkposition ELV reached (PP_EL)
   Safety-Endswitch ELV MINUS active (SS_EL_MINUS)
   Safety-Endswitch ELV PLUS active (SS_EL_PLUS)
   Endswitch DC ELV MINUS active (ED_EL_MINUS)
   Endswitch DC ELV PLUS active (ED_EL_PLUS)
   Endswitch Servo ELV MINUS active (ES_EL_MINUS)
   Endswitch Servo ELV PLUS active (ES_EL_PLUS)
   Back-Direction Emergency Drive AZM active (BD_AZM)
   Endswitch Servo AZM PLUS active (ES_AZM_PLUS)
   Endswitch DC AZM PLUS active (ED_AZM_PLUS)
   Endswitch DC AZM MINUS active (ED_AZM_MINUS)
   Safety-Endswitch AZM PLUS active (SS_AZM_PLUS)
   Safety-Endswitch AZM MINUS active (SS_AZM_MINUS)
   Pre-Parkposition AZM reached (PP_AZM)
   Warning Servo ELV
   Parkposition AZM reached (PP_AZM_MASTER)
   Ready Servo ELV
   U_d Minimum Voltage Servo ELV DC Power-Bus
   Holding Brake AZM Manual Open activated
   Holding Brake Servo AZM powered (open)
   Holding Brake ELV Manual Open activated
   Holding Brake Servo ELV powered (open)
   U_d Minimum Voltage Servo AZM DC Power-Bus
   Ready Servo AZM
   Warning Servo AZM
   Endposition Locking Device Forward (Closed) active
   Endposition Locking Device Backward (Open) active
   Locking Device Motor is pushing forward (Closing)
   Locking Device Motor is pulling backward (Opening)
   Camera Shelter Roof Open
   Locking Device push forward (close)
   Locking Device pull back (open)
   Error Reset Servo ELV
   Error Reset Servo AZM
   Servo Non-Halt ELV
   Probe T2
   Probe T1
   Servo AZM Enable
   Servo Non-Halt AZM
   Servo ELV Enable

   Example use:
   \code{.cpp}
   DigitalIOHandler digital_io_handler( TelescopeId_CT1 );
   digital_io_handler.TriggerChannel( "some channel" );
   if( digital_io_handler.IsChannelTriggered( "some channel: )
   std::cout << "It worked!" << std::endl;
   \endcode

   \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_DIGITALIOHANDLER_H_
#define ONLINETRACKING_DIGITALIOHANDLER_H_

#include <vector>

#include "IOModule.hh"
#include "SocketConnection.hh"
#include "DBHandler.hh"

namespace tracking {

class DigitalIOHandler {

 public:
  /// Reads the configuration of the io channels for the telescope
  /// 'telescope_id' from the HESS data base
  /// and connects to the hardware of this telescope. Throws if either fail.
  explicit DigitalIOHandler(const TelescopeId &telescope_id);
  ~DigitalIOHandler();

  /// Triggers the channel channel_name.
  /// Throws if any step in the process fails.
  void TriggerChannel(const std::string &channel_name);
  /// Untriggers the channel channel_name.
  /// Throws if any step in the process fails.
  void UnTriggerChannel(const std::string &channel_name);
  /// Reads the state of the channel channel_name and returns
  /// true if it is triggered. Throws if any step in the process
  /// fails.
  const bool IsChannelTriggered(const std::string &channel_name);
  /// Reads the state of the channel channel_name and returns true if
  /// it is untriggered. Throws if any step in the process failed.
  const bool IsChannelUnTriggered(const std::string &channel_name);
  /// Sets the timeout of the watchdog to timeout_seconds and starts
  /// the countdown.
  void EnableWatchdog(const float &timeout_seconds);
  /// Stops the countdown of the watchdog.
  void DisableWatchdog();
  /// Sets the countdown timer of the watchdog to the value it was enabled with.
  void PingWatchdog();
  /// Returns true if the watchdog has timed out.
  bool HasWatchdogTimedOut();
  /// TODO
  void ResetWatchdog();

  // DoIOModuleCheck()
  // CheckForChannelStateChanges()

  /// Prints the channel names, logic and current values to std::cerr.
  void PrintAllChannels();

 private:
  /// The copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  DigitalIOHandler(const DigitalIOHandler &);
  void operator=(const DigitalIOHandler &);

  /// Creates all IOModules and assigns them their properties
  /// according to the configuration in the data base table Drive_IOmod_conf.
  /// Throws if the expected information couldnt be retreived from the database
  /// or if an IOModule could not be constructed with the information given.
  void SetupIOModules();
  /// Adds channels to each module and gives them a name, logic and channel
  /// number, according to the configuration in the data base table
  /// Drive_IOmod_ChList
  /// Throws if the expected information couldnt be retrieved from the data
  /// base.
  void AssignChannelsToModules();
  /// Checks that the correct module type responds at each module address.
  /// Throws if the communication to the module failed or if a wrong
  /// module type was found.
  void ConfirmModuleConfig();
  /// Checks that each module has the expected number of channels assigned.
  /// Throws if not.
  void ConfirmChannelAssignments();
  /// Reads the channel values of an IOModule and puts the information in the
  /// appropriate IOModule object.
  void ReadIOModule(IOModule *io_module);
  /// Does the above but for all modules.
  void ReadAllIOModules();
  /// Returns the IOModule with the number 'number', or throws if such
  /// a module couldnt be found.
  IOModule *GetIOModule(const int &number);
  /// Returns the IOModule that contains the channel 'channel_name' or
  /// throws if such a channel couldnt be found in any module.
  IOModule *GetIOModule(const std::string &channel_name);
  /// Converts a moduletype given as a string to a ModuleType enum.
  /// Returns ModuleType_Invalid if no corresponding type was found.
  const ModuleType GetModuleType(const std::string &type) const;

  TelescopeId telescope_id_;  /// the handler connects to this telescope
  std::vector<IOModule> io_modules_;
  SocketConnection socket_connection_;
  DBHandler db_handler_;
};

}  // namespace

#endif
