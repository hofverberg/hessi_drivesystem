/**
   \class tracking::IOModuleAnswer

   Base class for all classes describing an answer from the IO modules to
   a command sent to them. All answers have the same construction, described in
   this base class:
   - a one char start character, which can be '!','?' or '>'
   - a body, which can be of various length
   - 2 checksum characters
   - a one char end character (always CR)

   \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_IOMODULEANSWER_H_
#define ONLINETRACKING_IOMODULEANSWER_H_

#include <vector>

namespace tracking {

class IOModuleAnswer {
 public:
  /// Assigns the start, body, checksum and end-characters from the given vector
  /// of chars. Validates the checksum. Throws if any of the above fails.
  explicit IOModuleAnswer(const std::vector<char> &answer);
  virtual ~IOModuleAnswer() { ; }
  /// Each derived command must define what a valid answer is, i.e which
  /// startcharacter is expected, how long the data buffer is, aso. This is
  /// included in this function.
  virtual const bool IsValid() const = 0;
  /// Each derived command must define where the data buffer
  /// is located in the command body (the body normally
  /// consists of the data buffer + some other stuff)
  virtual const std::vector<char> GetData() const = 0;

 protected:
  const char GetStartCharacter() const;
  const std::vector<char> GetBody() const;
  const std::size_t GetExpectedBodyLength(
      const std::size_t &full_command_length) const;

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  IOModuleAnswer(const IOModuleAnswer &handler);
  IOModuleAnswer &operator=(const IOModuleAnswer &handler);
  /// The command was successfully received by the IO module,
  /// but it didnt recognise it.
  const bool CommandNotRecognised() const;
  /// Checks if the header and footer is any of the allowed ones.
  const bool IsHeaderAndFooterValid(const char &start_character,
                                    const char &end_character) const;
  const bool IsChecksumValid(const std::vector<char> &data,
                             const char &checksum_one,
                             const char &checksum_two) const;

  // this is the answer format
  char start_character_;
  std::vector<char> body_;
  char checksum_[2];
  char end_character_;
};

}  // namespace

#endif
