/**
   \file Utils.hh

   Utility functions.

   \author Petter Hofvererg
*/

#ifndef ONLINETRACKING_UTILS_H_
#define ONLINETRACKING_UTILS_H_

#include <vector>
#include <string>

namespace tracking {

// todo(Hofverberg): Move to the ErrorHandler when possible
class IOError {
 public:
  const std::string GetMessage() const { return message_; }
  explicit IOError(const std::string &message) { message_ = message; }

 private:
  std::string message_;
};

namespace utils {

const std::string VectorOfCharToString(const std::vector<char> &data);
const std::vector<char> StringToVectorOfChar(const std::string &data);
/// Converts a positive integer to a vector of upper case hex characters.
const std::vector<char> IntToVectorOfHex(const int &value);
/// Converts a vector of hex characters to an int. Throws if a non-HEX
/// character was given. Overflow not checked!
const int VectorOfHexToInt(const std::vector<char> &data);
/// Returns true if the vector of chars only contains hex characters (0-9,a-f or
/// A-F).
const bool IsHex(const std::vector<char> &data);
/// Calcutes a 2 hex char checksum for a vector of chars of any length.
/// Algorithm:
/// hex1 = ( integer sum of all characters >> 4) & 0xF
/// hex2 = integer sum of all characters & 0xF
void CalculateChecksum(const std::vector<char> &data, char *checksum_one,
                       char *checksum_two);

/// Convert a Hex number ( dec: 0-15, hex: 0x0 to 0xF) to the ASCII value that
/// character (i.e 0..9, A..F) represents.
const int ConvertHexToASCIIValue(const int &value);
const std::string HandleError();

}  // namespace utils
}  // namespace tracking

#endif
