/**
   \class tracking::IOModuleCommand

   Base class for all classes describing an IO module command.
   All commands have the same construction, described in this base class:
   - a one char start character, which can be '>', '~'
   - a body, which can be of various length
   - an optional 2 char checksum
   - a one char end character (always CR)

   The checksum is calculated from the start character and the body. The
   algorithm can
   be found in tracking::Utils. This is the same algorithm as the IO module
   hardware is using
   when calculating the checksum for the answers they send back.

   \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_IOMODULECOMMAND_H_
#define ONLINETRACKING_IOMODULECOMMAND_H_

#include <vector>
#include <string>

#include "Constants.hh"

namespace tracking {

class IOModuleCommand {
 public:
  virtual ~IOModuleCommand() { ; }
  /// Add two checksum characters to the command
  void EnableChecksum();
  /// Do not add checksum characters to the command
  void DisableChecksum();
  /// Assembles a command from the start character, body, checksum and end
  /// character
  /// and returns it.
  const std::vector<char> GetFormattedCommand() const;
  /// Get the expected length of an answer from the IO modules to this command
  /// assuming everything went well.
  virtual const int GetExpectedReplyLength(
      const ModuleType &module_type = ModuleType_Invalid) const = 0;

 protected:
  /// Assigns the start character and the body from the argument. Throws if this
  /// couldnt be done.
  /// IOModuleCommands should only be created using the factory method, the
  /// constructor is therefore private.
  explicit IOModuleCommand(const std::string &command);
  const std::vector<char> GetBody() const;
  const char GetStartCharacter() const;

  bool add_checksum_;

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  IOModuleCommand(const IOModuleCommand &handler);
  IOModuleCommand &operator=(const IOModuleCommand &handler);

  // this is the command format
  char start_character_;
  std::vector<char> body_;
  char checksum_[2];
  char end_character_;
};

}  // namespace

#endif
