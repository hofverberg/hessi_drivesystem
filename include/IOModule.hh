/**
   \class tracking::IOModule

   Describes an I/O Module used in the CT1-4 drive systems. Each telescope is
   equipped with 6 io modules which are responsible for the digital io channels.
   Three different types of io modules are used: 7067, 7067D and 7053D. The
  first
  two have 7 channels, the third 16.

  An io module is identified through its number (1-6), its address (1-4,11,12)
  and
  its type.

  \author Petter Hofvererg
*/

#ifndef ONLINETRACKING_IOMODULE_H_
#define ONLINETRACKING_IOMODULE_H_

#include <vector>
#include <string>

#include "Constants.hh"
#include "IOChannel.hh"

namespace tracking {

class IOModule {

 public:
  /// @param number 1-6.
  /// @param address physical address of the module. Can be 1-4,11, or 12.
  /// @param type module type (7067,7067D,7053D).
  /// @param id unused.
  /// @param max_number_of_channel max number of channels allowed for this
  /// module.
  IOModule(const int &number, const int &address, const ModuleType &type,
           const std::string &id, const int &max_number_of_channels);
  ~IOModule();

  /// Tries to add a new channel to the module. Throws if it fails.
  /// @param number number of the channel (0-15 7053D, 0-6 7067/7067D). Must be
  /// unique.
  /// @param name name of the channel. Must be unique.
  /// @param logic positive (1) or negative (0) logic of the channel?
  void AddChannel(const int &number, const std::string &name, const int &logic);
  /// checks if a channel with the name [name] exists in the module.
  const bool DoesChannelExist(const std::string &name);
  /// triggers the channel with name [name]. Throws if channel doesnt exist.
  void TriggerChannel(const std::string &name);
  /// untriggers the channel with anme [name]. Throws if channel doesnt exist.
  void UnTriggerChannel(const std::string &name);
  /// returns true if channel [name] is triggered. Throws if channel doesnt
  /// exist.
  const bool IsChannelTriggered(const std::string &name);
  /// returns true if channel [name] is untriggered. Throws if channel doesnt
  /// exist.
  const bool IsChannelUnTriggered(const std::string &name);
  /// get the channel values as a vector of booleans where the vector index
  /// corresponds
  /// to the channel number.
  const std::vector<bool> GetChannelValues() const;
  /// set the channel values to the values contained in the vector of bools,
  /// where the vector
  /// index corresponds to the channel number.
  void SetChannelValues(const std::vector<bool> &ch_values);

  const int GetNumber() const;
  const int GetAddress() const;
  const ModuleType GetType() const;
  const std::string GetId() const;
  const int GetMaxNumberOfChannels() const;
  const int GetNumberOfChannels() const;

  void Print() const;

 private:
  /// returns true if the channel with the number [number] exists.
  const bool DoesChannelExist(const int &number);
  /// returns the channel with number [number] or throws if it doesnt exist.
  IOChannel *GetChannel(const int &number);
  /// returns the channel with name [name] or throws if it doesnt exist.
  IOChannel *GetChannel(const std::string &name);

  int number_;
  int address_;
  ModuleType module_type_;
  std::string id_;  /// unused
  int max_n_channels_;
  std::vector<IOChannel> io_channels_;
};

}  // namespace

#endif
