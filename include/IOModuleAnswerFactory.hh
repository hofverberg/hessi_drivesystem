/**
   \class tracking::IOModuleAnswerFactory

   A factory to create a user specified derived class of IOModuleAnswer from a
   vector
   of chars (normally read by a socket connection to a hardware device).

   This is a variant of the factory design pattern.

   Example usage:
   \code{.cpp}
   std::vector<std::char> read_buffer;
   socket_connection.read(10, read_buffer);
   IOModuleAnswer *answer = IOModuleAnswerFactory::Create(&read_buffer,
   CommandType_SearchModule);
   \endcode

   \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_IOMODULEANSWERFACTORY_H_
#define ONLINETRACKING_IOMODULEANSWERFACTORY_H_

#include "Constants.hh"
#include "IOModuleAnswer.hh"

namespace tracking {

class IOModuleAnswerFactory {

 public:
  ~IOModuleAnswerFactory() { ; }

  /// Create an IOModuleAnswer from the given data. Throws if any of the input
  /// parameters are unvalid.
  /// @param answer data read from a hardware device
  /// @param command_data the command to which's answer it will try to convert
  /// the vector<char> to
  static IOModuleAnswer *Create(const std::vector<char> &answer,
                                const CommandType &command_type);

 private:
  /// This class should never be instantiated, but only used through its static
  /// Create function.
  IOModuleAnswerFactory() { ; }
};

}  // namespace

#endif
