/**
      \class tracking::IOModuleCommandFactory

      A factory to create a user specified derived class of IOModuleCommand. The
   use must give
      the arguments required for the command to be constructed, which can be for
   example the address of the
      module the command should be sent to

      Example usage:
      \code{.cpp}
      int moduleAddress = 3;
      IOModuleCommand *answer =
   IOModuleCommandFactory::Create(CommandType_SearchModule, moduleAddress)
      \endcode

      This is a variant of the factory design pattern.

      \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_IOMODULECOMMANDFACTORY_H_
#define ONLINETRACKING_IOMODULECOMMANDFACTORY_H_

#include <vector>

#include "IOModuleCommand.hh"

namespace tracking {

class IOModuleCommandFactory {

 public:
  ~IOModuleCommandFactory() { ; }

  /// Create an IOModuleCommand from the given parameters. Throws if any of the
  /// parameters are unvalid.
  static IOModuleCommand *Create(const CommandType &command_type);
  /// Create an IOModuleCommand from the given parameters. Throws if any of the
  /// parameters are unvalid.
  static IOModuleCommand *Create(const CommandType &command_type,
                                 const int &module_address);
  /// Create an IOModuleCommand from the given parameters. Throws if any of the
  /// parameters are unvalid.
  static IOModuleCommand *Create(const CommandType &command_type,
                                 const int &module_address,
                                 const std::vector<char> &argument);
  /// Create an IOModuleCommand from the given parameters. Throws if any of the
  /// parameters are unvalid.
  static IOModuleCommand *Create(const CommandType &command_type,
                                 const int &module_address,
                                 const ModuleType &module_type,
                                 const std::vector<char> &argument);

 private:
  IOModuleCommandFactory() { ; }
};

}  // namespace

#endif
