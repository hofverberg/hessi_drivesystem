/**
   \class tracking::ConnectionGuard

   The class closes a GenericConnection when
   its destructor is called. Use this class as a
   scope guard to ensure that a connection is closed
   even if an exception was thrown in the function body.
   \code{.cpp}
   {
   GenericConnection myConnection;
   ConnectionGuard myConnectionGuard( &myConnection );
   myConnection.Connect();
   }
   // connection is now closed
   \endcode

   \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_CONNECTIONGUARD_H_
#define ONLINETRACKING_CONNECTIONGUARD_H_

#include "GenericConnection.hh"

namespace tracking {

class ConnectionGuard {
 public:
  /// the connection which will be closed in the destructor,
  /// which must derive from GenericConnection, must be passed
  /// in in the constructor
  explicit ConnectionGuard(GenericConnection* connection);
  /// closes the connection of the GenericConnection
  ~ConnectionGuard();

 private:
  // the copy constructor and assignment operator are declared
  // but have no definition to avoid them from being (mis)used.
  ConnectionGuard(const ConnectionGuard&);
  void operator=(const ConnectionGuard&);

  GenericConnection* generic_connection_;
};

}  // namespace

#endif
