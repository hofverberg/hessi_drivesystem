/**
  \class tracking::SocketConnection

  Wrapper class for opening/closing a connection to an ethernet port
  and write/read data to/from it.

  Uses the package stcp developed by Thomas Kihm.

  \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_SOCKETCONNECTION_H_
#define ONLINETRACKING_SOCKETCONNECTION_H_

#include <vector>
#include <string>

#include "GenericConnection.hh"
#include "stcp.hh"

namespace tracking {

enum e_ConnectionType {
  ConnectionType_Client = 0,
  ConnectionType_Server = 1
};

class SocketConnection : public GenericConnection {
 public:
  /// @param connection_type either client or server
  /// @param ip IP number of the host/client to connect to.
  /// @param port Port number of the host/client to connect to.
  /// @param connection_timeout timeout value in ms for waiting for incoming
  /// connections. 0 means wait forever.
  /// @param read_timeout timeout value in ms for reading the requested number
  /// of bytes. 0 means wait forever.
  /// @param write_timeout timeout value in ms for writing the requested number
  /// of bytes. 0 means wait forever.
  SocketConnection(const e_ConnectionType &connection_type,
                   const std::string &ip, const std::string &port,
                   const int &connection_timeout, const int &read_timeout,
                   const int &write_timeout);
  ~SocketConnection();

  void Connect();
  virtual void Deconnect();
  /// Returns true if a successful Connect() attempt has previously been made,
  /// and Deconnect() has not been called.
  /// Does not check the actual state of the connection.
  const bool IsConnected() const;

  /// Reads [n_chars] bytes from the socket and puts the chars in [read_buffer].
  /// Rhrows if not connected or if the read request timed out.
  void Read(const int &n_chars, std::vector<char> *read_buffer);
  /// Write all chars in [write_buffer] to the socket.
  /// Throws if not connected or if the write request timed out.
  void Write(const std::vector<char> &write_buffer);

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  SocketConnection(const SocketConnection &socket);
  SocketConnection &operator=(const SocketConnection &socket);

  e_ConnectionType connection_type_;
  Stcp socket_;
  std::string ip_;
  std::string port_;
  int connection_timeout_;  /// [ms]
  int read_timeout_;        /// [ms]
  int write_timeout_;       /// [ms]
  bool is_connected_;
};

}  // namespace

#endif
