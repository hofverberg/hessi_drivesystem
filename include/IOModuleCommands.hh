/**
   \file IOModuleCommands

   Contains all classes deriving from IOModuleCommand.
   Each class defines a specific command that can be
   sent to the IO Modules to request information or
   to set some parameters. The generic structure of all
   commands is inherited from IOModuleCommand, and each
   sub-class defines the specific implementation of a single
   command.

   These classes should only be instantitated via the IOModuleCommandFactory.

  \author Petter Hofverberg
*/

#ifndef ONLINETRACKING_IOMODULECOMMANDS_H_
#define ONLINETRACKING_IOMODULECOMMANDS_H_

#include <vector>
#include <string>

#include "IOModuleCommand.hh"

namespace tracking {

/**
   \class tracking::SearchModuleCommand

   Checks if a module is connected at address [module_address]
   and if so, the answer contains the type of the module that was found.
*/
class SearchModuleCommand : public IOModuleCommand {
 public:
  explicit SearchModuleCommand(const int &module_address);
  ~SearchModuleCommand() { ; }

  /// The length of the reply depends on the module type since
  /// the answer string contains the module type which can vary.
  const int GetExpectedReplyLength(const ModuleType &module_type =
                                       ModuleType_Invalid) const;

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  SearchModuleCommand(const SearchModuleCommand &handler);
  SearchModuleCommand &operator=(const SearchModuleCommand &handler);
  /// Assembles and returns the command string from the argument
  static const std::string FormatCommandBody(const int &module_address);
};

/**
\class tracking::ReadModuleHardwareConfigCommand

Read parameter like baudrate, .. for the module with address [module_address]
TODO
*/
class ReadModuleHardwareConfigCommand : public IOModuleCommand {
 public:
  explicit ReadModuleHardwareConfigCommand(const int &module_address);
  ~ReadModuleHardwareConfigCommand() { ; }

  const int GetExpectedReplyLength(const ModuleType &module_type =
                                       ModuleType_Invalid) const {
    return 8;
  }

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  ReadModuleHardwareConfigCommand(
      const ReadModuleHardwareConfigCommand &handler);
  ReadModuleHardwareConfigCommand &operator=(
      const ReadModuleHardwareConfigCommand &handler);
  /// Assembles and returns the command string from the argument
  static const std::string FormatCommandBody(const int &module_address);
};

/**
   \class tracking::GetChannelValuesCommand

   Requests the channel values (1 or 0) of all channels of the module
   with the address [module_address].
*/
class GetChannelValuesCommand : public IOModuleCommand {
 public:
  explicit GetChannelValuesCommand(const int &module_address);
  ~GetChannelValuesCommand() { ; }

  const int GetExpectedReplyLength(const ModuleType &module_type =
                                       ModuleType_Invalid) const {
    return 8;
  }

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  GetChannelValuesCommand(const GetChannelValuesCommand &handler);
  GetChannelValuesCommand &operator=(const GetChannelValuesCommand &handler);
  /// Assembles and returns the command string from the argument
  static const std::string FormatCommandBody(const int &module_address);
};

/**
   \class tracking::SetChannelValuesCommand

   Set the channel values (1 or 0) of the module at address [module_address].
*/
class SetChannelValuesCommand : public IOModuleCommand {
 public:
  /// @param module_type the module type at address [module_address]
  /// @param module_address the address to which the command will be sent
  /// @param channel_bit_set the channel values as hex characters.
  SetChannelValuesCommand(const ModuleType &module_type,
                          const int &module_address,
                          const std::vector<char> &channel_bit_set);
  ~SetChannelValuesCommand() { ; }

  const int GetExpectedReplyLength(const ModuleType &module_type =
                                       ModuleType_Invalid) const {
    return 4;
  }

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  SetChannelValuesCommand(const SetChannelValuesCommand &handler);
  SetChannelValuesCommand &operator=(const SetChannelValuesCommand &handler);
  /// Assembles and returns the command string from the arguments.
  /// For the arguments, see comment for Constructor.
  static const std::string FormatCommandBody(
      const ModuleType &module_type, const int &module_address,
      const std::vector<char> &channel_bit_set);
};

/**
\class tracking::PingWatchdogCommand

Ping all watchdogs, i.e restart the countdown from its maximum value.
This command has no answer
*/
class PingWatchdogCommand : public IOModuleCommand {
 public:
  PingWatchdogCommand();
  ~PingWatchdogCommand() { ; }

  const int GetExpectedReplyLength(const ModuleType &module_type =
                                       ModuleType_Invalid) const {
    return 0;
  }

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  PingWatchdogCommand(const PingWatchdogCommand &handler);
  PingWatchdogCommand &operator=(const PingWatchdogCommand &handler);
  /// Assembles and returns the command string.
  static const std::string FormatCommandBody();
};

/**
\class tracking::CheckWatchdogCommand

Checks if the watchdog at address [module_address] has timed out or not.
*/
class CheckWatchdogCommand : public IOModuleCommand {
 public:
  explicit CheckWatchdogCommand(const int &module_address);
  ~CheckWatchdogCommand() {
    ;
  };

  const int GetExpectedReplyLength(const ModuleType &module_type =
                                       ModuleType_Invalid) const {
    return 8;
  }

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  CheckWatchdogCommand(const CheckWatchdogCommand &handler);
  CheckWatchdogCommand &operator=(const CheckWatchdogCommand &handler);
  /// Assembles and returns the command string from the argument.
  static const std::string FormatCommandBody(const int &module_address);
};

/**
\class tracking::ResetWatchdogCommand

Reset the watchdog at address [module_address].
If a watchdog timed out, it needs to be reset before action can recommence.
*/
class ResetWatchdogCommand : public IOModuleCommand {
 public:
  explicit ResetWatchdogCommand(const int &module_address);
  ~ResetWatchdogCommand() { ; }

  const int GetExpectedReplyLength(const ModuleType &module_type =
                                       ModuleType_Invalid) const {
    return 6;
  }

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  ResetWatchdogCommand(const ResetWatchdogCommand &handler);
  ResetWatchdogCommand &operator=(const ResetWatchdogCommand &handler);
  /// Assembles and returns the command string from the argument.
  static const std::string FormatCommandBody(const int &module_address);
};

/**
\class tracking::EnableWatchdogCommand

Enables the watchdog at address [module_address] and sets the timeout value.
The watchdog starts the countdown immediately after the reception of this
command.
*/
class EnableWatchdogCommand : public IOModuleCommand {
 public:
  /// @param module_address the address to where send the command
  /// @param timeout_seconds_x10 The timeout value x10. The timeout value can be
  /// between 0.1 and 25.5 s, and the (confusing) argument timeout_seconds thus
  /// has a range between 1 and 255.
  EnableWatchdogCommand(const int &module_address,
                        const std::vector<char> &timeout_seconds_x10);
  ~EnableWatchdogCommand() { ; }

  const int GetExpectedReplyLength(const ModuleType &module_type =
                                       ModuleType_Invalid) const {
    return 6;
  }

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  EnableWatchdogCommand(const EnableWatchdogCommand &handler);
  EnableWatchdogCommand &operator=(const EnableWatchdogCommand &handler);
  /// Assembles and returns the command string from the arguments.
  /// For a description of the arguments, see the comment for the Constructor.
  static const std::string FormatCommandBody(
      const int &module_address, const std::vector<char> &timeout_seconds_x10);
};

/**
\class tracking::DisableWatchdogCommand

Disable watchdog (i.e stop the countdown permanently, or until another enable
command has been sent) and set timeout value to 0.1s
*/
class DisableWatchdogCommand : public IOModuleCommand {
 public:
  explicit DisableWatchdogCommand(const int &module_address);
  ~DisableWatchdogCommand() { ; }

  const int GetExpectedReplyLength(const ModuleType &module_type =
                                       ModuleType_Invalid) const {
    return 6;
  }

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  DisableWatchdogCommand(const DisableWatchdogCommand &handler);
  DisableWatchdogCommand &operator=(const DisableWatchdogCommand &handler);
  /// Assembles and returns the command string from the arguments.
  static const std::string FormatCommandBody(const int &module_address);
};

/*
  \class tracking::TestCommand

  Use for unit testing of IOModuleCommand.
 */
class TestCommand : public IOModuleCommand {
 public:
  explicit TestCommand(const std::string &command);
  ~TestCommand() { ; }

  const int GetExpectedReplyLength(const ModuleType &module_type =
                                       ModuleType_Invalid) const {
    return 0;
  }

 private:
  /// the copy constructor and assignment operator are declared
  /// but have no definition to avoid them from being (mis)used.
  TestCommand(const TestCommand &handler);
  TestCommand &operator=(const TestCommand &handler);
};

}  // namespace

#endif
