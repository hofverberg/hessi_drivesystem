/**
   \class tracking::IOChannel

   See the header file for a description of this class.

   \author Petter Hofverberg
 */

#include "IOChannel.hh"

#include <assert.h>
#include <iostream>
#include <sstream>

#include "Constants.hh"
#include "Utils.hh"

namespace tracking {

IOChannel::IOChannel(const int &number, const std::string &name,
                     const int &logic)
    : number_(number), name_(name) {
  if (name.size() < 4)
    throw IOError("IOChannel name must be atleast 4 characters");

  if (number < 0) throw IOError("IOChannel number cannot be negative");

  switch (logic) {
    case 0:
      this->logic_ = Logic_Negative;
      this->value_ = Value_Set;
      break;
    case 1:
      this->logic_ = Logic_Positive;
      this->value_ = Value_UnSet;
      break;
    default:
      std::stringstream message;
      message << logic << " is not a valid channel logic";
      throw IOError(message.str());
      break;
  }
}

IOChannel::~IOChannel() {}

const int IOChannel::GetNumber() const { return this->number_; }

const std::string IOChannel::GetName() const { return this->name_; }

void IOChannel::SetValue() { this->value_ = Value_Set; }

const bool IOChannel::IsValueSet() const { return (this->value_ == Value_Set); }

const bool IOChannel::IsValueUnSet() const {
  return (this->value_ == Value_UnSet);
}

void IOChannel::UnSetValue() { this->value_ = Value_UnSet; }

const bool IOChannel::HasPositiveLogic() const {
  return (this->logic_ == Logic_Positive);
}

const bool IOChannel::HasNegativeLogic() const {
  return (this->logic_ == Logic_Negative);
}

void IOChannel::Print() const {
  std::cerr << " Channel Number " << this->number_ << " Name " << this->name_
            << " Logic " << this->logic_ << " Value " << this->value_
            << std::endl;
}

}  // namespace
