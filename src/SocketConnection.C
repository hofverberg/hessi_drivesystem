/**
   \class tracking::SocketConnection

   See the header file for a description of this class.

   \author Petter Hofverberg
*/

#include "SocketConnection.hh"

#include <stdint.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "Constants.hh"
#include "Utils.hh"

namespace tracking {

SocketConnection::SocketConnection(const e_ConnectionType &connection_type,
                                   const std::string &ip,
                                   const std::string &port,
                                   const int &connection_timeout,
                                   const int &read_timeout,
                                   const int &write_timeout)
    : connection_type_(connection_type),
      ip_(ip),
      port_(port),
      connection_timeout_(connection_timeout),
      read_timeout_(read_timeout),
      write_timeout_(write_timeout),
      is_connected_(false) {}

SocketConnection::~SocketConnection() {
  if (this->IsConnected()) this->Deconnect();
}

void SocketConnection::Connect() {
  if (!this->IsConnected()) {
    std::string adress_string = ip_ + ":" + port_;
    socket_ = StcpInit((char *)adress_string.c_str(), connection_type_);

    if (socket_ == 0) throw IOError(std::string("Could not initialize socket"));

    int return_code = StcpWait(socket_, connection_timeout_);

    switch (return_code) {
      case 1:
        std::cerr << "Successfully connected to port " << port_ << std::endl;
        break;
      case 0:
        StcpShutdown(socket_);
        throw IOError(std::string("Could not connect to port ") + port_);
        break;
      case 2:
        StcpShutdown(socket_);
        throw IOError(std::string("Timeout connecting to port ") + port_);
        break;
      default:
        StcpShutdown(socket_);
        throw IOError(std::string(
                          "Unknown return value from StcpWait while connecting "
                          "to port ") +
                      port_);
        break;
    }
  }  // if not connected

  this->is_connected_ = true;
}

void SocketConnection::Deconnect() {
  if (this->IsConnected()) {
    int return_code = StcpShutdown(socket_);

    if (return_code == 1)
      std::cerr << "Successfully deconnected from port " << port_ << std::endl;
    else
      std::cerr << "Somehow failed to deconnected from port " << port_
                << std::endl;
  } else {
    std::cerr
        << "Deconnect was called even though there was no connection to port "
        << port_ << std::endl;
  }

  is_connected_ = false;
}

const bool SocketConnection::IsConnected() const { return is_connected_; }

void SocketConnection::Read(const int &n_chars,
                            std::vector<char> *read_buffer) {
  if (!this->IsConnected())
    throw IOError(std::string("Trying to read from socket when not connected"));

  char data_buffer[n_chars];

  int n_read = StcpRead(socket_, n_chars, &data_buffer[0], read_timeout_);

  if (n_read == n_chars) {
    std::cerr << "Successfully read " << n_read << " bytes" << std::endl;
    for (int i_char = 0; i_char < n_chars; ++i_char)
      read_buffer->push_back(data_buffer[i_char]);
  } else {
    std::stringstream message;
    message << "Only read " << n_read << " bytes out of " << n_chars
            << std::endl;
    throw IOError(message.str());
  }
}

void SocketConnection::Write(const std::vector<char> &write_buffer) {
  if (!this->IsConnected())
    throw IOError(std::string("Trying to write to socket when not connected"));

  if (write_buffer.size() == 0)
    throw IOError(std::string("write_buffer is empty, cannot write anything."));

  const int n_to_write = write_buffer.size();

  char data_buffer[n_to_write];
  for (int i_char = 0; i_char < n_to_write; ++i_char)
    data_buffer[i_char] = write_buffer[i_char];

  int n_sent =
      StcpWrite(socket_, n_to_write, (void *)&data_buffer[0], write_timeout_);
  if (n_sent == n_to_write) {
    std::cerr << "Successfully wrote " << n_sent << " bytes" << std::endl;
  } else {
    std::stringstream message;
    message << "Only wrote " << n_sent << " bytes out of " << n_to_write
            << std::endl;
    throw IOError(message.str());
  }
}

}  // namespace
